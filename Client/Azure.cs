﻿using Timer = System.Timers.Timer;

namespace AzureRemoteService;

public static class Extensions
{
	public const string DEBUG_PREFIX = "Test";

	public static bool IsDebug( this string account )
	{
		var P = Math.Max( account.LastIndexOf( '/' ), account.LastIndexOf( '\\' ) );

		if( P >= 0 )
			// ReSharper disable once ReplaceSubstringWithRangeIndexer
			account = account.Substring( P + 1 );

		return account.StartsWith( DEBUG_PREFIX, StringComparison.OrdinalIgnoreCase ) || account.EndsWith( DEBUG_PREFIX, StringComparison.OrdinalIgnoreCase );
	}
}

public partial class Azure
{
	public const string PRODUCTION_END_POINT = "https://idsroute.azurewebsites.net",
	                    ALPHA_TRW_END_POINT  = "https://alphatrw.azurewebsites.net",
	                    ALPHAC_END_POINT     = "https://alphacjt.azurewebsites.net",
	                    BETA_END_POINT       = "https://idsroute-beta.azurewebsites.net",
	                    ROWAN_END_POINT      = "https://idsweb-alphar.azurewebsites.net",
	                    SCHEDULES_END_POINT  = "https://idsroute-schedules.azurewebsites.net";

	public const int ENCRYPTION_TIMEOUT_IN_SECONDS = 7 * 24 * 60 * 60; // 7 Days

	public const string TIME_ERROR = "TimeError";

	public static bool NoListeners
	{
		get
		{
			lock( LockObject )
				return _NoListeners;
		}

		set
		{
			lock( LockObject )
				_NoListeners = value;
		}
	}

	public static bool DebugServer { get; set; }

	private static bool _NoListeners;

#if NET9_0_OR_GREATER
	private static readonly Lock LockObject = new();
#else
	private static readonly object LockObject = new();
#endif
	public enum AZURE_CONNECTION_STATUS
	{
		ERROR,
		TIME_ERROR,
		OK
	}

	public enum SLOT
	{
		PRODUCTION,
		ALPHAC,
		BETA,
		ALPHA_TRW,
		ALPHAR,
		SCHEDULES,
		FORCE_PRODUCTION,
		CARRIER_BASED
	}

	public class InternalEndpointClient : IdsClient
	{
		protected readonly string EndPoint;

		public InternalEndpointClient( string endPoint ) : base( endPoint )
		{
			EndPoint = endPoint;
		}

		public static string GetEndpoint( SLOT slot, string? carrierId )
		{
			switch( slot )
			{
			case SLOT.ALPHA_TRW:
				return ALPHA_TRW_END_POINT;

			case SLOT.SCHEDULES:
				return SCHEDULES_END_POINT;

			case SLOT.ALPHAC:
				return ALPHAC_END_POINT;

			case SLOT.BETA:
				return BETA_END_POINT;

			case SLOT.ALPHAR:
				return ROWAN_END_POINT;

			case SLOT.FORCE_PRODUCTION:
				return PRODUCTION_END_POINT;
			}

			if( carrierId is not null )
			{
				carrierId = carrierId.TrimToUpper();

				if( carrierId != "" )
				{
					var P = carrierId.IndexOf( '/' );

					if( P >= 0 )
						// ReSharper disable once ReplaceSubstringWithRangeIndexer
						carrierId = carrierId.Substring( P + 1 );

					string DoTest()
					{
						const string TEST = "TEST";

						return carrierId.StartsWith( TEST, StringComparison.OrdinalIgnoreCase )
						       || carrierId.EndsWith( TEST, StringComparison.OrdinalIgnoreCase )
							       ? ALPHA_TRW_END_POINT : PRODUCTION_END_POINT;
					}

					return carrierId switch
					       {
						       ""             => PRODUCTION_END_POINT, // For initial ping
						       "CORAL"        => ALPHA_TRW_END_POINT,
						       "TERRY"        => ALPHA_TRW_END_POINT,
						       "LOOPBACKTEST" => ALPHA_TRW_END_POINT,
						       "CHRISTOPHER"  => ALPHAC_END_POINT,
						       "PMLPLAY"      => SCHEDULES_END_POINT,
						       "PML"          => SCHEDULES_END_POINT,
						       _              => DoTest()
					       };
				}
			}
			return PRODUCTION_END_POINT;
		}

		public static string GetEndpoint( string? carrierId ) => GetEndpoint( Slot, carrierId );
	}

	public class InternalClient : InternalEndpointClient
	{
		public DateTimeOffset SignInTime { get; private set; } = DateTimeOffset.MinValue;

		public string AuthToken
		{
			set
			{
				if( string.IsNullOrEmpty( value ) )
					throw new ArgumentException( "Auth token set to empty" );

				lock( ReLoginLockObject )
					AuthorisationToken = value;
			}

			get
			{
				if( ReLoginTimer is null )
				{
					ReLoginTimer = new Timer( 24 * 60 * 60 * 1000 ) { AutoReset = true };

					ReLoginTimer.Elapsed += async ( _, _ ) =>
					                        {
						                        try
						                        {
							                        var NewToken = await Client.RequestReLogin();

							                        if( NewToken is not null )
							                        {
								                        var (Value, Ok) = Encryption.FromTimeLimitedToken( NewToken );

								                        if( Ok )
								                        {
									                        lock( ReLoginLockObject )
										                        AuthorisationToken = Value;
								                        }
							                        }
						                        }
						                        catch
						                        {
						                        }
					                        };
					ReLoginTimer.Start();
				}

				lock( ReLoginLockObject )
					return AuthorisationToken;
			}
		}

		// ReSharper disable once MemberHidesStaticFromOuterClass
	#if NET9_0_OR_GREATER
		private static readonly Lock LockObject = new();
	#else
		private static readonly object LockObject = new();
	#endif

		public readonly string ProgramName,
		                       CarrierId,
		                       UserName;

		private bool SignedIn;

		public InternalClient( SLOT slot, string programName, string carrierId, string userName ) : base( DoGetEndpoint( slot, carrierId ) )
		{
			ProgramName          =   programName;
			CarrierId            =   carrierId;
			UserName             =   userName;
			GetAuthTokenCallback ??= s => Encryption.ToTimeLimitedToken( s, ENCRYPTION_TIMEOUT_IN_SECONDS );
		}

		public InternalClient( string programName, string carrierId, string userName ) : this( Slot, programName, carrierId, userName )
		{
		}

		public async Task SignOut()
		{
			if( SignedIn )
			{
				try
				{
					var Diff = DateTimeOffset.UtcNow - SignInTime;

					await RequestSignOut( new SignOut
					                      {
						                      Hours   = (uint)Diff.Hours,
						                      Minutes = (byte)Diff.Minutes,
						                      Seconds = (byte)Diff.Seconds
					                      } ).ConfigureAwait( false );
				}
				finally
				{
					SignedIn = false;
				}
			}
		}

		public async Task<AZURE_CONNECTION_STATUS> LogIn( string password, bool isDriver = false )
		{
			var IsDebug  = CarrierId.IsDebug();
			var TicksNow = DateTime.UtcNow.Ticks.ToString();

			Func<string, string, string, string, string, string, string, string, Task<string>> Func = isDriver ? RequestLoginAsDriver : RequestLogin;

			var Result = await Func( ProgramName, Encryption.ToTimeLimitedToken( CarrierId ),
			                         Encryption.ToTimeLimitedToken( UserName ),
			                         Encryption.ToTimeLimitedToken( password ),
			                         DateTimeOffset.Now.Offset.Hours.ToString(),
			                         isDriver ? "Driver" : "User",
			                         IsDebug ? "t" : "f",
			                         Encryption.ToTimeLimitedToken( TicksNow ) );

			if( Result != TIME_ERROR )
			{
				if( Result.IsNotNullOrWhiteSpace() )
				{
					var (Value, Ok) = Encryption.FromTimeLimitedToken( Result );

					if( Ok )
					{
						if( Value.IsNotNullOrWhiteSpace() )
						{
							AuthToken  = Value;
							SignInTime = DateTimeOffset.UtcNow;
							SignedIn   = true;

							DebugServer = IsDebug;

							return AZURE_CONNECTION_STATUS.OK;
						}
						return AZURE_CONNECTION_STATUS.ERROR;
					}
					return AZURE_CONNECTION_STATUS.TIME_ERROR;
				}
				return AZURE_CONNECTION_STATUS.ERROR;
			}
			return AZURE_CONNECTION_STATUS.TIME_ERROR;
		}

		private static string DoGetEndpoint( SLOT slot, string carrierId )
		{
			lock( LockObject )
			{
				var SaveSlot = Slot;
				Slot = slot;
				var Temp = GetEndpoint( carrierId );
				Slot = SaveSlot;
				return Temp;
			}
		}

	#if !NO_LISTENERS
		private CancellationTokenSource? CancellationTokenSource;

		public void CancelListenTripUpdate()
		{
			if( CancellationTokenSource is { } Cancellation )
			{
				try
				{
					Cancellation.Cancel();
					CancellationTokenSource = null;
				}
				catch
				{
				}
				Cancellation.Cancel();
			}
		}

		public void ListenTripUpdate( string connectionType, string userName, string timeToLiveHours, Action<List<TripUpdateMessage>> response )
		{
			if( !NoListeners )
				CancellationTokenSource = ListenTripUpdate( EndPoint, AuthToken, connectionType, userName, timeToLiveHours, response );
		}
	#endif

	#region ReSign In
	#if NET9_0_OR_GREATER
		private readonly Lock ReLoginLockObject = new();
	#else
		private readonly object ReLoginLockObject = new();
	#endif
		private Timer? ReLoginTimer;
	#endregion
	}

#region Slot
	private static SLOT _Slot = SLOT.PRODUCTION;

	// ReSharper disable once ConvertToAutoProperty
	public static SLOT Slot
	{
		get => _Slot;
		set => _Slot = value;
	}
#endregion
}