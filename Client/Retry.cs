﻿namespace AzureRemoteService;

public partial class Azure
{
	public const int RETRY_DELAY = 10_000;

	public static async Task<bool> Retry( Func<Task<bool>> action, int maxRetries = 10, int delayMs = RETRY_DELAY )
	{
		var RetryCount = 0;

		while( true )
		{
			try
			{
				var Ok = await action();

				if( Ok )
					return true;
			}
			catch
			{
			}

			if( RetryCount >= maxRetries )
				return false;

			RetryCount++;
			await Task.Delay( delayMs );
		}
	}
}