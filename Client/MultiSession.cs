// ReSharper disable MemberHidesStaticFromOuterClass

namespace AzureRemoteService;

public partial class Azure
{
	public class MultiSession
	{
		public static  MultiSession  Instance => _Instance ??= new MultiSession();
		public static  MultiSession  Client   => Instance;
		private static MultiSession? _Instance;

		private static readonly MemoryCache<string, InternalClient> Cache = new();

		private static readonly TimeSpan DefaultTimeToLive = TimeSpan.FromHours( 1 );

		public static async Task<bool> LogIn( SLOT slot, TimeSpan timeToLive, string programName, string userName, string carrierId, string password, bool isDriver = false )
		{
			carrierId = carrierId.TrimToCapitalise();
			userName  = userName.TrimToCapitalise();

			var InCache = Cache.TryGetValue( carrierId, out var InternalClient );

			if( !InCache )
				InternalClient = new InternalClient( slot, programName, carrierId, userName );

			AZURE_CONNECTION_STATUS Result;

			if( carrierId != "" )
				Result = await InternalClient.LogIn( password, isDriver );
			else
				Result = AZURE_CONNECTION_STATUS.OK;

			if( Result == AZURE_CONNECTION_STATUS.OK )
			{
				if( !InCache )
					Cache.AddSliding( carrierId, InternalClient, timeToLive, async ( _, client ) =>
					                                                         {
						                                                         await client.SignOut();
						                                                         client.Dispose();
					                                                         } );
				return true;
			}
			return false;
		}


		public static Task<bool> LogIn( TimeSpan timeToLive, string programName, string userName, string carrierId, string password, bool isDriver = false ) => LogIn( SLOT.CARRIER_BASED, timeToLive, programName, userName, carrierId, password, isDriver );
		public static Task<bool> LogIn( string programName, string userName, string carrierId, string password, bool isDriver = false ) => LogIn( DefaultTimeToLive, programName, userName, carrierId, password, isDriver );

		public static Task<bool> LogInDelayed( SLOT slot, TimeSpan timeToLive, string programName, string userName, string carrierId, string password, bool isDriver = false )
		{
			RandomDelay.Delay();
			return LogIn( slot, timeToLive, programName, userName, carrierId, password, isDriver );
		}

		public static Task<bool> LogInDelayed( TimeSpan timeToLive, string programName, string userName, string carrierId, string password, bool isDriver = false ) => LogIn( timeToLive, programName, userName, carrierId, password, isDriver );

		public static Task<bool> LogInDelayed( string programName, string userName, string carrierId, string password, bool isDriver = false ) => LogIn( programName, userName, carrierId, password, isDriver );

		// Used by the Schedules
		public static Task<bool> LogInDelayed( string programName, SLOT slot = SLOT.SCHEDULES ) => LogInDelayed( slot, DefaultTimeToLive, programName, "", "", "" );

		public InternalClient? this[ string carrierId ] => Cache.TryGetValue( carrierId.TrimToCapitalise(), out var Value ) ? Value : null;
	}
}