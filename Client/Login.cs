﻿using Newtonsoft.Json;

namespace AzureRemoteService;

using LOGIN_CLIENT = (Azure.AZURE_CONNECTION_STATUS Status, Azure.InternalClient Client);

public partial class Azure
{
	public static async Task<LOGIN_CLIENT> LogInNoGlobal( InternalClient client, string password, bool isDriver = false )
	{
		var Status = await client.LogIn( password, isDriver );

		if( Status != AZURE_CONNECTION_STATUS.OK )
		{
			client.Dispose();
			return ( Status, null! );
		}

		return ( Status, client );
	}

	public static Task<LOGIN_CLIENT> LogInNoGlobal( string pgm, string carrierId, string userName, string password, bool isDriver = false ) => LogInNoGlobal( new InternalClient( pgm, carrierId, userName ), password, isDriver );

	public static async Task<LOGIN_CLIENT> LogIn( string pgm, string carrierId, string userName, string password, bool isDriver = false )
	{
		var Result = await LogInNoGlobal( pgm, carrierId, userName, password, isDriver ).ConfigureAwait( false );

		if( Result.Status == AZURE_CONNECTION_STATUS.OK )
			Client = Result.Client;

		return Result;
	}

	public static async Task<(AZURE_CONNECTION_STATUS Status, string CarrierId, string UserName, string AccountId, bool Enabled)> LogIn( string pgm,
	                                                                                                                                     string loginCode, string password,
	                                                                                                                                     bool debug = false )
	{
		var Lc     = Encryption.ToTimeLimitedToken( loginCode.Trim() );
		var Pw     = Encryption.ToTimeLimitedToken( password );
		var Offset = DateTimeOffset.Now.Offset.Hours.ToString();

		var Result = await Client.RequestTwoCodeLogin( pgm, Lc, Pw, Offset, debug.ToString() ); // Returns CustomerLogin as encrypted JSON string
		var Info   = Encryption.FromTimeLimitedToken( Result );

		if( Info.Ok )
		{
			var Login = JsonConvert.DeserializeObject<CustomerLogin>( Info.Value );

			if( Login is { Ok: true } )
			{
				Client.AuthToken = Login.AuthToken;
				return ( AZURE_CONNECTION_STATUS.OK, Login.CarrierId, Login.UserName, Login.AccountId, Login.Enabled );
			}
		}
		return ( AZURE_CONNECTION_STATUS.ERROR, "", "", "", false );
	}

#region Client
	public static InternalClient Client
	{
		get => GetClient( ref _Client );
		set => SetClient( ref _Client, value );
	}

	public static InternalClient SecondaryClient
	{
		get => GetClient( ref _SecondaryClient );
		set => SetClient( ref _SecondaryClient, value );
	}

	private static InternalClient? _Client,
	                               _SecondaryClient;

#if NET9_0_OR_GREATER
	private static readonly Lock ClientLock = new();
#else
	private static readonly object ClientLock = new();
#endif

	private static InternalClient GetClient( ref InternalClient? client )
	{
		lock( ClientLock )
			return client ??= new InternalClient( "", "", "" ); // For Initial Ping
	}

	// ReSharper disable once RedundantAssignment
	private static void SetClient( ref InternalClient? client, InternalClient? value )
	{
		lock( ClientLock )
		{
			var OldClient = client;

			if( OldClient is not null )
			{
				// Wait 10 minutes before disposing the old client
				Tasks.RunVoid( 10 * 60_000, () =>
				                            {
					                            try
					                            {
						                            OldClient.Dispose();
					                            }
					                            catch
					                            {
					                            }
				                            } );
			}
			client = value;
		}
	}
#endregion
}