﻿{ignoreCRLF}
{if $HAS_FORWARDERS}
{\s}	{if $HAS_EXPLICIT_FORWARDERS}
{\s}		{foreach $PAIR in $EXPLICIT_FORWARDERS}{assign $VALUE = $PAIR.Value}
			public abstract byte[] {$VALUE.Name}Forwarder( string path, HttpRequest request, HttpResponse response );{\n}
{\s}		{/foreach}
{\s}	{/if}
{\s}	{if $HAS_DEFAULT_FORWARDERS}
{\s}		{foreach $PAIR in $DEFAULT_FORWARDERS}{assign $VALUE = $PAIR.Value}
			public abstract byte[] {$VALUE.Name}Forwarder( string path, HttpRequest request, HttpResponse response );{\n}
{\s}		{/foreach}
{\s}	{/if}
{\n}
{/if}
{\s}	{if $REQUIRES_AUTHORISATION}
			public abstract bool ValidateAuthorisation( string authToken );{\n}
{/if}
{/ignoreCRLF}

{foreach $PROTOCOL in $PROTOCOLS}{if $PROTOCOL.Summary}
{$PROTOCOL.Summary}
{/if}{ignoreCRLF}
{assign $UPLOAD_VOID = $PROTOCOL.IsUpload ? " HttpFileCollection files " : ""}
{assign $UPLOAD = $PROTOCOL.IsUpload ? ", HttpFileCollection files " : ""}
{assign $IS_LISTNER = $PROTOCOL.IsListener}
{assign $NAME = $PROTOCOL.RealName}
{capture $RESPONSE_TYPE}{if $IS_LISTNER}List<{$PROTOCOL.RealName}Packet>{else}{$PROTOCOL.ResponseType}{/if}{/capture}
{capture $RESPONSE_NAME}{if $IS_LISTNER}ResponseListen{else}Response{/if}{$NAME}{/capture}
{if !$PROTOCOL.IsRequestVoid}
			public abstract {$RESPONSE_TYPE} {$RESPONSE_NAME}( {$PROTOCOL.RequestType} requestObject{$UPLOAD} );
{elseif $PROTOCOL.IsCommand}
			public abstract {$RESPONSE_TYPE} {$RESPONSE_NAME}( {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}string {$ARG}{assign $FIRST = 0}{/foreach}{$UPLOAD} );
{else}
			public abstract {$RESPONSE_TYPE} {$RESPONSE_NAME}({$UPLOAD_VOID});
{/if}
{if $IS_LISTNER}
{capture $OBJECT_TYPE}{if $PROTOCOL.IsCommand}{assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}string {$ARG}{assign $FIRST = 0}{/foreach}, List<{$PROTOCOL.ResponseType}>{else}{$RESPONSE_TYPE}{/if}{/capture}
{\n}			public abstract	bool ResponseListen{$NAME}Ack( List<long> idList );{\n}
			public abstract bool Response{$NAME}Receive{$PROTOCOL.ResponseType}( {$OBJECT_TYPE} receivedObject );{\n}
{/if}
{/ignoreCRLF}
{/foreach}