﻿{ignoreCRLF}
{if $HAS_LISTENERS}
{\s}	{foreach $PROTOCOL in $PROTOCOLS}
{\s}		{if $PROTOCOL.IsListener && $PROTOCOL.IsCommand}
		public class Send{$PROTOCOL.RealName}{$PROTOCOL.ResponseType}Container{\n}
		{{{\n}
			public Dictionary<string, string> A { get; set; }{\n}
			public List<{$PROTOCOL.ResponseType}> D { get; set; }{\n}
		}{\n}{\n}

		public class {$PROTOCOL.RealName}Packet{\n}
		{{{\n}
			public long Id { get; set; }{\n}
			public {$PROTOCOL.ResponseType} Data { get; set; }{\n}
		}{\n}
{\n}
{\s}		{/if}
{\s}	{/foreach}
{/if}
		public interface IServer{\n}
		{{{\n}
{if $HAS_FORWARDERS}
{\s}	{if $HAS_EXPLICIT_FORWARDERS}
{\s}		{foreach $PAIR in $EXPLICIT_FORWARDERS}{assign $VALUE = $PAIR.Value}
			byte[] {$VALUE.Name}Forwarder( string path, HttpRequest request, HttpResponse response );{\n}
{\s}		{/foreach}
{\s}	{/if}
{\s}	{if $HAS_DEFAULT_FORWARDERS}
{\s}		{foreach $PAIR in $DEFAULT_FORWARDERS}{assign $VALUE = $PAIR.Value}
			byte[] {$VALUE.Name}Forwarder( string path, HttpRequest request, HttpResponse response );{\n}
{\s}		{/foreach}
{\s}	{/if}
{\n}
{/if}


{\s}	{if $REQUIRES_AUTHORISATION}
			bool ValidateAuthorisation( string authToken );{\n}
{/if}
{/ignoreCRLF}

{foreach $PROTOCOL in $PROTOCOLS}{if $PROTOCOL.Summary}
{$PROTOCOL.Summary}
{/if}{ignoreCRLF}
{assign $UPLOAD_VOID = $PROTOCOL.IsUpload ? " HttpFileCollection files " : ""}
{assign $UPLOAD = $PROTOCOL.IsUpload ? ", HttpFileCollection files " : ""}
{assign $IS_LISTNER = $PROTOCOL.IsListener}
{assign $NAME = $PROTOCOL.RealName}
{capture $RESPONSE_TYPE}{if $IS_LISTNER}List<{$PROTOCOL.RealName}Packet>{else}{$PROTOCOL.ResponseType}{/if}{/capture}
{capture $RESPONSE_NAME}{if $IS_LISTNER}ResponseListen{else}Response{/if}{$NAME}{/capture}
{if !$PROTOCOL.IsRequestVoid}
			{$RESPONSE_TYPE} {$RESPONSE_NAME}( {$PROTOCOL.RequestType} requestObject{$UPLOAD} );
{elseif $PROTOCOL.IsCommand}
			{$RESPONSE_TYPE} {$RESPONSE_NAME}( {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}string {$ARG}{assign $FIRST = 0}{/foreach}{$UPLOAD} );
{else}
			{$RESPONSE_TYPE} {$RESPONSE_NAME}({$UPLOAD_VOID});
{/if}
{if $IS_LISTNER}
{capture $OBJECT_TYPE}{if $PROTOCOL.IsCommand}{assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}string {$ARG}{assign $FIRST = 0}{/foreach}, List<{$PROTOCOL.ResponseType}>{else}{$RESPONSE_TYPE}{/if}{/capture}
{\n}			bool ResponseListen{$NAME}Ack( List<long> idList );{\n}
			bool Response{$NAME}Receive{$PROTOCOL.ResponseType}( {$OBJECT_TYPE} receivedObject );{\n}
//{\n} 
{/if}
{/ignoreCRLF}
{/foreach}
		}
