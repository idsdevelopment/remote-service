﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using PulsarV3;
using SendReceive.Parser.Visitors;

// ReSharper disable InconsistentNaming

namespace SendReceiveV2;

public static class Extensions
{
	public static string ToIdent( this string value )
	{
		var First  = true;
		var Retval = new StringBuilder();

		foreach( var C in value )
		{
			if( C is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or '_' )
				Retval.Append( C );

			else if( !First && C is >= '0' and <= '9' )
				Retval.Append( C );
			else
				Retval.Append( '_' );

			First = false;
		}

		return Retval.ToString();
	}

	public static string Capitalise( this string value ) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase( value.ToLower() );
}

internal class Program
{
	private const string VERSION = "4.5.01";

	private static Assembly Assembly;

	private static readonly char[] HashChars =
	[
		'K', // 0
		'b',
		'V',
		's',
		'9',
		'e',
		'M',
		'2',
		'P',
		'N',

		'I', // 10
		'z',
		'7',
		'm',
		'Q',
		'k',
		'u',
		'R',
		'5',
		'A',

		'1', //20
		'c',
		'd',
		'T',
		'_',
		'G',
		'Z',
		'x',
		'a',
		'r',

		'0', //30
		'g',
		'L',
		'n',
		'E',
		'j',
		'H',
		'U',
		'f',
		'X',

		'B', //40
		'3',
		'h',
		'D',
		'i',
		'4',
		'8',
		'F',
		'o',
		'p',

		'Y', //50
		'q',
		'W',
		'S',
		'O',
		'J',
		'v',
		'w',
		'C',
		'y',

		't', //60
		'l',
		'6' //62
	];

	private static (Dictionary<string, Forwarder> Explicit, Dictionary<string, Forwarder> Default) ForwarderNames( IEnumerable<List<Forwarder>> forwarders )
	{
		var Result = ( Explicit: new Dictionary<string, Forwarder>(), Default: new Dictionary<string, Forwarder>() );

		foreach( var Forwarders in forwarders )
		{
			foreach( var Forwarder in Forwarders )
			{
				var F = Forwarder.Name.Trim().ToLower().Trim( '.' );

				// Default
				if( F.StartsWith( "*." ) )
				{
					F              = F[ 2.. ];
					Forwarder.Name = $"{F.ToIdent().Capitalise()}_Default_";
					Result.Default.Add( F, Forwarder );
				}
				else
				{
					Forwarder.Name = $"{F.ToIdent().Capitalise()}_";
					Result.Explicit.Add( F, Forwarder );
				}
			}
		}

		return Result;
	}

	private static void Main( string[] args )
	{
		Console.WriteLine( $"\r\nSendReceiveV2 Version: {VERSION}" );
		var IsTest   = false;
		var ExitCode = 0;
		var IsCore   = false;

		try
		{
			string InFile  = null,
			       OutPath = null;

			var IsInFile   = true;
			var WasOutPath = false;

			var IsHashed = false;

			static void BadCommand()
			{
				throw new Exception( "Incorrect command line args" );
			}

			if( args.Length == 0 )
				BadCommand();

			foreach( var Arg in args )
			{
				switch( Arg.Trim().ToUpper() )
				{
				case "/TEST" when IsInFile:
					InFile     = "./TestData/Test.proto";
					OutPath    = Path.GetDirectoryName( InFile );
					IsTest     = true;
					WasOutPath = true;
					break;

				case "/CORE":
					IsCore = true;
					break;

				case "/HASH":
					IsHashed = true;
					break;

				default:
					if( WasOutPath )
						BadCommand();

					else if( IsInFile )
					{
						IsInFile = false;
						InFile   = Arg;
						OutPath  = Path.GetDirectoryName( InFile );
					}
					else
					{
						WasOutPath = true;
						OutPath    = Arg;
					}
					break;
				}
			}

			Console.WriteLine( $"Input File: {Path.GetFullPath( InFile! )}" );
			Console.WriteLine( $"Output Path: {Path.GetFullPath( OutPath! )}" );

			var Parser = new SendReceiveParser.SendReceive();
			var Result = Parser.Parse( File.ReadAllText( InFile! ) );

			if( Parser.HasErrors )
			{
				foreach( var Error in Parser.ErrorList )
					Console.WriteLine( Error );

				ExitCode = 900;
			}

			if( ExitCode == 0 )
			{
				if( IsHashed )
				{
					static string HashIt( string name )
					{
						var Result = new StringBuilder();

						byte Mask = 0xff;

						foreach( var C in name )
						{
							byte Ndx = C switch
							           {
								           >= '0' and <= '9' => (byte)( (byte)C - (byte)'0' ) // 0..9
								          ,
								           >= 'a' and <= 'z' => (byte)( ( (byte)C - (byte)'a' ) + 10 ) // 10..35
								          ,
								           >= 'A' and <= 'Z' => (byte)( ( (byte)C - (byte)'A' ) + 36 ) // 36..61
								          ,
								           '_' => 62,
								           _   => 0
							           };

							var Ch = HashChars[ ( Ndx ^ Mask ) % 63 ];

							unchecked
							{
								Mask--;
							}

							Result.Append( Ch );
						}
						return Result.ToString();
					}

					foreach( var Protocol in Result.Protocols.Protocols )
						Protocol.HashedName = HashIt( Protocol.RealName );
				}

				var Pulsar = new Pulsar();

				string GetTemplate( string template, bool register = true )
				{
					Assembly ??= Assembly.GetEntryAssembly();

					// ReSharper disable once PossibleNullReferenceException
					var Template = Assembly.GetManifestResourceStream( $"SendReceiveV2.Resources.Templates.{template}" );
					var Text     = new StreamReader( Template ?? throw new InvalidOperationException() ).ReadToEnd();

					if( register )
						Pulsar.RegisterTemplate( template, Text );

					return Text;
				}

				GetTemplate( "AutoGenerated.Tpl" );
				GetTemplate( "ServerInterface.Tpl" );
				GetTemplate( "AbstractServerInterface.Tpl" );
				GetTemplate( "ServerActions.Tpl" );
				GetTemplate( "ClientActions.Tpl" );
				GetTemplate( "ClientListener.Tpl" );

				Pulsar.Assign( "DOT_NET", IsCore );

				Pulsar.Assign( "DATE_TIME", DateTimeOffset.Now.ToString( "s" ) );
				Pulsar.Assign( "VERSION_NUMBER", VERSION );
				Pulsar.Assign( "GENERATED_FROM", InFile );
				Pulsar.Assign( "NAME", Result.Name );
				Pulsar.Assign( "USINGS", Result.Usings );
				Pulsar.Assign( "SETTINGS", Result.Setting );
				Pulsar.Assign( "MAX_REQUESTS_BEFORE_BLOCKING", Result.Setting.MaxRequests );
				Pulsar.Assign( "MAX_CONNECTIONS", Result.Setting.MaxConnections );
				Pulsar.Assign( "REQUEST_TIMEOUT", Result.Setting.RequestTimeout );
				Pulsar.Assign( "EXECUTION_TIMEOUT", Result.Setting.ExecutionTimeout );
				Pulsar.Assign( "REQUIRES_AUTHORISATION", Result.Protocols.AuthorisationRequired );
				Pulsar.Assign( "PROTOCOLS", Result.Protocols.Protocols );
				Pulsar.Assign( "RETRIES", Result.Setting.Retries );

				Pulsar.Assign( "NO_AUTHORISATION_PROTOCOLS", Result.Protocols.NoAuthorisationList );
				Pulsar.Assign( "HAS_NO_AUTHORISATION_PROTOCOLS", Result.Protocols.NoAuthorisationList.Count > 0 );

				Pulsar.Assign( "HAS_LISTENERS", Result.Protocols.Protocols.Any( result => result.IsListener ) );

				var (Explicit, Default) = ForwarderNames( Result.Forwarders );
				Pulsar.Assign( "EXPLICIT_FORWARDERS", Explicit );
				Pulsar.Assign( "DEFAULT_FORWARDERS", Default );
				Pulsar.Assign( "HAS_FORWARDERS", Result.Forwarders.Count > 0 );
				Pulsar.Assign( "HAS_EXPLICIT_FORWARDERS", Explicit.Count > 0 );
				Pulsar.Assign( "HAS_DEFAULT_FORWARDERS", Default.Count > 0 );

				Pulsar.Assign( "HAS_UPLOADS", ( from P in Result.Protocols.Protocols
				                                where P.IsUpload
				                                select P ).Any() );

				var ServerCode = Pulsar.Fetch( GetTemplate( "Server.Tpl", false ) );

				if( Pulsar.HasErrors )
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine( "--- Server Template ---" );

					foreach( var Error in Pulsar.SyntaxErrors )
						Console.WriteLine( Error );

					ExitCode = 901;
				}

				if( ExitCode == 0 )
				{
					var ClientCode = Pulsar.Fetch( GetTemplate( "Client.Tpl" ) );

					if( Pulsar.HasErrors )
					{
						Console.ForegroundColor = ConsoleColor.DarkRed;
						Console.WriteLine( "--- Client Template ---" );

						foreach( var Error in Pulsar.SyntaxErrors )
							Console.WriteLine( Error );

						ExitCode = 901;
					}

					if( ExitCode == 0 )
					{
						string Server,
						       Client;

						if( IsTest )
						{
							Server = "TestServer.cs";
							Client = "TestClient.cs";
						}
						else
						{
							Server = $"{Result.Name}Server.cs";
							Client = $"{Result.Name}Client.cs";
						}

						var OutFile = Path.GetFullPath( Path.Combine( OutPath!, Server ) );
						File.WriteAllText( OutFile, ServerCode );

						OutFile = Path.GetFullPath( Path.Combine( OutPath, Client ) );
						File.WriteAllText( OutFile, ClientCode );
					}
				}
			}
		}
		catch( Exception E )
		{
			Console.ForegroundColor = ConsoleColor.DarkYellow;
			Console.WriteLine( E.Message );
			ExitCode = 900;
		}

		Environment.ExitCode = ExitCode;

		Console.ResetColor();

	#if !DEBUG
		if( ExitCode != 0 )
		{
			while( !Console.KeyAvailable )
				Thread.Sleep( 250 );
		}
	#else
		while( !Console.KeyAvailable )
			Thread.Sleep( 250 );
	#endif
	}
}