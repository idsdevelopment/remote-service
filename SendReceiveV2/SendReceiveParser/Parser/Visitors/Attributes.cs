﻿using System;

namespace SendReceive.Parser;

internal partial class SendReceiveVisitors
{
	public override Result VisitProtocolObjectAttribute( SendReceiveParser.ProtocolObjectAttributeContext context )
	{
		var Summary = (ProtocolSummary)VisitProtocolSummary( context.protocolSummary() );

		var Result = new ProtocolAttribute
		             {
			             Summary = Summary.Summary
		             };

		var A    = context.ATTRIBUTE();
		var Name = "";

		if( A is not null )
		{
			var Txt = A.GetText().Trim();

			var Ndx = Txt.IndexOf( ':' );

			if( Ndx >= 0 )
			{
				Name = Txt[ ..Ndx ];
				Txt  = Txt[ ( Ndx + 1 ).. ];

				if( Txt.Contains( "upload", StringComparison.OrdinalIgnoreCase ) )
					Result.IsUpload = true;

				if( Txt.Contains( "download", StringComparison.OrdinalIgnoreCase ) )
					Result.IsDownload = true;
			}
			else
				Name = Txt;
		}

		Result.Name = Name;

		var Auth = context.AUTH_TOKEN();

		if( Auth != null )
		{
			Result.IsAuthorisationAttribute = true;
			Result.Name                     = Auth.GetText().Trim();
		}
		else
			Result.NoAuthorisationRequired = context.NO_AUTH_TOKEN() != null;

		return Result;
	}

	public override Result VisitProtocolAttribute( SendReceiveParser.ProtocolAttributeContext context ) => new AuthorisationAttribute { AuthorisationRequired = context.AUTH_TOKEN() != null };
}