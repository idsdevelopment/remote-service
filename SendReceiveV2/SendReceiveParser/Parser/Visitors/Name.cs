﻿using System;

namespace SendReceive.Parser;

internal partial class SendReceiveVisitors
{
	internal class NoNameException : ArgumentException
	{
		internal NoNameException() : base( "Name not specified" )
		{
		}
	}

	public override Result VisitName( SendReceiveParser.NameContext context )
	{
		var Id = context.IDENT();
		if( Id == null )
			throw new NoNameException();

		var N = Id.GetText().Trim();
		if( string.IsNullOrWhiteSpace( N ) )
			throw new NoNameException();

		return new NameResult { Name = N };
	}
}