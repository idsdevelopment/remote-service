﻿namespace SendReceive.Parser;

public static class ParserExtensions
{
	public static string ToNumber( this string num ) => num.Trim().Replace( "_", "" );
}

internal partial class SendReceiveVisitors
{
	public override Result VisitSettings( SendReceiveParser.SettingsContext context )
	{
		var Result   = new SettingsResult();
		var Settings = context.setting();

		if( Settings is not null )
		{
			foreach( var Setting in Settings )
			{
				var MaxR = Setting.MAX_REQUESTS();

				if( MaxR is not null )
				{
					if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var MaxRequests ) )
						Result.Settings.MaxRequests = MaxRequests;
				}
				else
				{
					var RTimeout = Setting.REQUEST_TIMEOUT();

					if( RTimeout is not null )
					{
						if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var RequestTimeout ) )
							Result.Settings.RequestTimeout = RequestTimeout;
					}
					else
					{
						var Page = Setting.PAGE();

						if( Page is not null )
						{
							var Id = Setting.IDENT();

							if( Id is not null )
								Result.Settings.Page = Id.GetText().Trim();
						}
						else
						{
							var Connections = Setting.MAX_CONNECTIONS();

							if( Connections is not null )
							{
								if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var MaxConnections ) )
									Result.Settings.MaxConnections = MaxConnections;
							}
							else
							{
								var ETimeout = Setting.EXECUTION_TIMEOUT();

								if( ETimeout is not null )
								{
									if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var ExecutionTimeout ) )
										Result.Settings.ExecutionTimeout = ExecutionTimeout;
								}
								else
								{
									var Rtys = Setting.RETRIES();

									if( Rtys is not null )
									{
										if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var Retries ) )
											Result.Settings.Retries = Retries;
									}
									else
									{
										var Delay = Setting.RETRY_DELAY();

										if( Delay is not null )
										{
											if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var RetryDelay ) )
												Result.Settings.RetryDelay = RetryDelay;
										}
										else
										{
											var Path = Setting.PATH();

											if( Path is not null )
											{
												var PName = Setting.PATH_NAME();

												if( PName is not null )
													Result.Settings.Path = PName.GetText().Trim();
											}
											else
											{
												var BPath = Setting.BASE_PATH();

												if( BPath is not null )
												{
													var PName = Setting.PATH_NAME();

													if( PName is not null )
														Result.Settings.BasePath = PName.GetText().Trim();
												}
												else
												{
													var LDelay = Setting.LISTENER_RETRY_DELAY();

													if( LDelay is not null )
													{
														if( int.TryParse( Setting.NUMBER().GetText().ToNumber(), out var ListenerRetryDelay ) )
															Result.Settings.ListenerRetryDelay = ListenerRetryDelay;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return Result;
	}
}