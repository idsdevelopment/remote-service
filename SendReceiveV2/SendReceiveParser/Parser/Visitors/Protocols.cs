﻿using System.Linq;

namespace SendReceive.Parser;

internal partial class SendReceiveVisitors
{
	public override Result VisitProtocolType( SendReceiveParser.ProtocolTypeContext context )
	{
		var Args       = context.args();
		var ArgsResult = Args is null ? new ArgsResult() : (ArgsResult)VisitArgs( context.args() );

		var Id = context.IDENT() ?? context.PROPERTY();

		return new ProtocolResult
		       {
			       Name = Id.GetText().Trim(),
			       Args = ArgsResult.Args
		       };
	}

	public override Result VisitProtocol( SendReceiveParser.ProtocolContext context )
	{
		var            Result = new ProtocolResult();
		ProtocolResult Type;

		var P = context.protocolType();

		if( P is not null )
		{
			Type             = (ProtocolResult)VisitProtocolType( P );
			Result.IsCommand = Type.Args.Count > 0;

			if( Result.IsCommand )
			{
				Result.Args        = Type.Args;
				Result.RequestType = "void";
			}
			else
				Result.RequestType = Type.Name;

			Result.Name = Type.Name;
		}

		var Action = context.action();

		if( Action is not null )
			Type = (ProtocolResult)VisitProtocolType( Action.protocolType() );
		else
		{
			var Listner = context.listener();

			if( Listner is not null )
			{
				Type              = (ProtocolResult)VisitListener( Listner );
				Result.IsListener = true;
			}
			else
				Type = new ProtocolResult();
		}

		Result.ResponseType = Type.Name;

		var Attributes = context.protocolObjectAttribute();

		if( Attributes != null )
			Result.Attribute = (ProtocolAttribute)VisitProtocolObjectAttribute( Attributes );
		else
			Result.Attribute = new ProtocolAttribute();

		if( !Result.Attribute.IsAuthorisationAttribute && !string.IsNullOrWhiteSpace( Result.Attribute.Name ) ) // Attribute name takes precedence
			Result.Name = Result.Attribute.Name;

		Result.IsRequestVoid  = ( Result.RequestType == "void" ) || Result.IsCommand;
		Result.IsResponseVoid = Result.ResponseType == "void";

		return Result;
	}

	public override Result VisitProtocols( SendReceiveParser.ProtocolsContext context )
	{
		var Result = new ProtocolsResult
		             {
			             AuthorisationRequired = ( (AuthorisationAttribute)VisitProtocolAttribute( context.protocolAttribute() ) ).AuthorisationRequired
		             };

		foreach( var ProtocolContext in context.protocol() )
			Result.Protocols.Add( (ProtocolResult)VisitProtocol( ProtocolContext ) );

		Result.Protocols = ( from P in Result.Protocols
		                     orderby P.IsListener descending, P.Name
		                     select P ).ToList();

		foreach( var Protocol in Result.Protocols )
		{
			if( Protocol.Attribute.NoAuthorisationRequired || Protocol.Attribute.IsAuthorisationAttribute )
				Result.NoAuthorisationList.Add( Protocol );
		}

		return Result;
	}
}