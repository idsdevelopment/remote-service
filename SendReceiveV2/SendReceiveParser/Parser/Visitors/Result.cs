﻿#nullable enable

namespace SendReceive.Parser.Visitors;

public class Result;

public class SettingEntry
{
	public string Page { get; set; } = "";

	public string Path     { get; set; } = "";
	public string BasePath { get; set; } = "";

	public int MaxRequests        { get; set; }
	public int RequestTimeout     { get; set; } = 60;
	public int MaxConnections     { get; set; }
	public int ExecutionTimeout   { get; set; }
	public int Retries            { get; set; }
	public int RetryDelay         { get; set; }
	public int ListenerRetryDelay { get; set; }
}

public class Forwarder
{
	public string Name     { get; set; } = "";
	public string MimeType { get; set; } = "text/plain";
}

public class ParseResult : Result
{
	public List<List<Forwarder>> Forwarders = [];
	public string                Name       = "";
	public ProtocolsResult       Protocols  = new();
	public SettingEntry          Setting    = new();
	public List<string>          Usings     = [];
}

public class NameResult : Result
{
	public string Name = "";
}

public class SettingsResult : Result
{
	public SettingEntry Settings = new()
	                               {
		                               Page     = "",
		                               Path     = "",
		                               BasePath = ""
	                               };
}

public class ForwarderResult : Result
{
	public List<List<Forwarder>> Forwarders = [];
}

public class UsingResult : Result
{
	public string Name = "";
}

public class UsingsResult : Result
{
	public List<string> Names = [];
}

public class AuthorisationAttribute : Result
{
	public bool AuthorisationRequired;
}

public class ProtocolSummary : Result
{
	public string Summary = "";
}

public class ProtocolAttribute : Result
{
	public bool IsAuthorisationAttribute,
	            NoAuthorisationRequired,
	            IsUpload,
	            IsDownload;

	public string Summary = "",
	              Name    = "";
}

public class ArgsResult : Result
{
	public List<string> Args = [];
}

public class ProtocolResult : Result
{
	public string Summary => Attribute.Summary;

	public bool         IsCommand    { get; set; }
	public List<string> Args         { get; set; } = [];
	public string       RequestType  { get; set; } = "";
	public string       ResponseType { get; set; } = "";

	public bool IsRequestVoid  { get; set; }
	public bool IsResponseVoid { get; set; }

	public bool IsAuthorisation => Attribute.IsAuthorisationAttribute;

	public bool IsListener { get; set; }

	public bool IsUpload   => Attribute.IsUpload;
	public bool IsDownload => Attribute.IsDownload;

	public ProtocolAttribute Attribute = new();

#region Name
	public string? HashedName { get; set; }
	public string  RealName   { get; set; } = "";

	public string Name
	{
		get => HashedName ?? RealName;
		set => RealName = value;
	}
#endregion
}

public class ProtocolsResult : Result
{
	public bool                 AuthorisationRequired;
	public List<ProtocolResult> NoAuthorisationList = [];
	public List<ProtocolResult> Protocols           = [];
}