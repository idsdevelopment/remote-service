﻿using System;
using System.Text;

namespace SendReceive.Parser;

internal partial class SendReceiveVisitors
{
	public override Result VisitProtocolSummary( SendReceiveParser.ProtocolSummaryContext context )
	{
		var Result = new ProtocolSummary();

		var Summary = context.SUMMARY();
		if( Summary != null )
		{
			var Temp = Summary.GetText().Trim().Split( new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries );

			var Sum = new StringBuilder();
			foreach( var S in Temp )
				Sum.Append( $"//  {S.Trim()}\r\n" );

			Result.Summary = Sum.ToString().Trim();
		}

		return Result;
	}
}