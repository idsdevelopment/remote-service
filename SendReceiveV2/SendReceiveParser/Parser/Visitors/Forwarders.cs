﻿using System;

namespace SendReceive.Parser;

internal partial class SendReceiveVisitors
{
	public override Result VisitForwarders( SendReceiveParser.ForwardersContext context )
	{
		var Result = new ForwarderResult();

		if( context is not null )
		{
			foreach( var Forwarder in context.forwarder() )
			{
				var Fords = new List<Forwarder>();
				Result.Forwarders.Add( Fords );

				var F = Forwarder.FORWARDER().GetText().Trim();

				var Types = F.Split( ',', StringSplitOptions.RemoveEmptyEntries );

				var Attribute = Forwarder.FORWARDER_ATTRIBUTE();
				var Attr      = Attribute?.GetText().Trim().Trim( '[', ']' ).Trim();

				// ReSharper disable once LoopCanBeConvertedToQuery
				foreach( var Type in Types )
				{
					var Frd = new Forwarder
					          {
						          Name = Type.Trim( ' ', '"', ';' )
					          };

					if( Attr is not null )
						Frd.MimeType = Attr;

					Fords.Add( Frd );
				}
			}
		}

		return Result;
	}
}