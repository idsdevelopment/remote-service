parser grammar SendReceiveParser;

options
	{ 
		tokenVocab=SendReceiveLexer; 		// Use tokens from lexer
	}	

parse: name settings? usings forwarders? protocols EOF
	;

usings: BEGIN_USING USING* END_USING
	;

using: USING
	;

name: NAME IDENT SEMICOLON
	;

forwarders: BEGIN_FORWARDERS forwarder* END_FORWARDERS
	;

forwarder: FORWARDER_ATTRIBUTE? FORWARDER
	;

protocols: protocolAttribute BEGIN_PROTOCOL protocol* END_PROTOCOL
	;

protocolAttribute: ( AUTH_TOKEN )?
    ;

protocol: protocolObjectAttribute protocolType ( action | listener ) SEMICOLON
	;

action: SEPARATOR protocolType
	;

listener: LISTENER protocolType
	;

protocolObjectAttribute: protocolSummary ( ( ATTRIBUTE ( COMMA  NO_AUTH_TOKEN )? ) | AUTH_TOKEN  )?
	;

protocolSummary: SUMMARY?
	;

protocolType: ( IDENT | PROPERTY ) args?
	;
	
args: BEGIN_ARGS ( (ARG COMMA)* ARG  )* END_ARGS
	 ;

settings: BEGIN_SETTINGS ( setting SEMICOLON )* END_SETTINGS
	;

setting:  MAX_REQUESTS NUMBER 
		| REQUEST_TIMEOUT NUMBER
		| AUTH_TOKEN
		| PAGE IDENT
		| MAX_CONNECTIONS NUMBER
		| EXECUTION_TIMEOUT NUMBER
		| RETRIES NUMBER
		| RETRY_DELAY NUMBER
		| LISTENER_RETRY_DELAY NUMBER
		| PATH PATH_NAME
		| BASE_PATH PATH_NAME
	;

