lexer grammar SendReceiveLexer;

	tokens
	{
		END_USING,
		END_PROTOCOL,
		END_SETTINGS,
		SEMICOLON,
		IDENT,
		PROPERTY,
		BEGIN_ARGS,
		COMMA,
		ARG,
		END_ARGS,
		ATTRIBUTE,
		MAX_REQUESTS,
		NUMBER,
		REQUEST_TIMEOUT,
		AUTH_TOKEN,
		NO_AUTH_TOKEN,
		PAGE,
		MAX_CONNECTIONS,
		EXECUTION_TIMEOUT,
		RETRIES,
		RETRY_DELAY,
		SUMMARY,
		PATH,
		BASE_PATH,
		PATH_NAME, 
		LISTENER_RETRY_DELAY,

		END_FORWARDERS
	}
	
	COMMENT : FCOMMENT																											-> skip;
	LINE_COMMENT : FLINE_COMMENT																								-> skip;
	 
	NAME : FSPACES 'name' FSPACES '=' FSPACES																					-> pushMode( ID_SEMI_MODE ); 
	BEGIN_USING : FSPACES 'usings' FSPACES '{'																					-> pushMode( USINGS_MODE );
	BEGIN_PROTOCOL : FSPACES 'protocol' FSPACES '{'																				-> pushMode( PROTOCOL_MODE );
	BEGIN_FORWARDERS : FSPACES 'forwarders' FSPACES '{'																			-> pushMode( FORWARDERS_MODE );
	BEGIN_SETTINGS : FSPACES 'settings' FSPACES '{'																				-> pushMode( SETTINGS_MODE );
	PROTOCOL_ATTR : FSPACES '['																									-> skip, pushMode( PROTOCOL_ATTRIBUTE_MODE );

mode ID_SEMI_MODE;
	 ID_SEMI_IDENT : FIDENT																										-> type( IDENT );
	 ID_SEMI_PROP : FPROPERTY																									-> type( PROPERTY );
	 ID_SEMICOLON : FSEMICOLON																									-> type( SEMICOLON ), popMode;

mode USINGS_MODE;
	 USING_COMMENT : FCOMMENT																									-> skip;
	 USING_LINE_COMMENT : FLINE_COMMENT																							-> skip;

	 USING : FSPACES FIDENT ( FSPACES '=' FSPACES FIDENT )? ( '.' FIDENT )* FSEMICOLON;
	 END_USING_MODE : FSPACES '}'																								-> type( END_USING ), popMode;
	 	
mode PROTOCOL_MODE;
	 PROTOCOL_COMMENT : FCOMMENT																								-> skip;
	 PROTOCOL_LINE_COMMENT : FLINE_COMMENT																						-> skip;
	 
	 PTOTOCOL_SUMMARY : FSUMMARY																								-> type( SUMMARY );
	 PROTOCOL_ATTRIBUTE : FSPACES '['																							-> skip, pushMode( ATTRIBUTE_MODE );
	 PROTOCOL_COMMAND : FSPACES '('																								-> type( BEGIN_ARGS ), pushMode( ARGS_MODE );
	 PROTOCOL_IDENT : FSPACES FIDENT FSPACES																					-> type( IDENT );
	 PROTOCOL_PROP : FSPACES FPROPERTY FSPACES																					-> type( PROPERTY );
	 LISTENER : FSPACES '<=';
	 SEPARATOR : FSPACES '=>' FSPACES '<=';
	 PROTOCOL_SEMICOLON : FSEMICOLON 																							-> type( SEMICOLON );
	 END_PROTOCOL_MODE : FSPACES '}'																							-> type( END_PROTOCOL ), popMode;

mode FORWARDERS_MODE;
	 FORWARDER: FSPACES FQUOTED_FORWARDER FSPACES ( ',' FSPACES FQUOTED_FORWARDER )* ';';	
	 FORWARDER_ATTRIBUTE: FSPACES '[' FSPACES FFORWARDER_ATTRIBUTE FSPACES ']';
	 END_FORWARDERS_MODE : FSPACES '}'																							-> type( END_FORWARDERS ), popMode;

mode ARGS_MODE;
	 ARGS_ARG : FSPACES FIDENT																									-> type( ARG );
	 ARGS_COMMA : FCOMMA																										-> type( COMMA );				
	 ARGS_END_MODE : FSPACES ')'																								-> type( END_ARGS ), popMode;

mode REQUEST_MODE;
	 REQUEST_CLASS : FSPACES FIDENT FSPACES;

mode SETTINGS_MODE;
	 SETTINGS_COMMENT : FCOMMENT																								-> skip;
	 SETTINGS_LINE_COMMENT : FLINE_COMMENT																						-> skip;

	 SETTINGS_EXECUTION_TIMEOUT : FSPACES 'ExecutionTimeout' FSPACES '='														-> type( EXECUTION_TIMEOUT );
	 SETTINGS_MAX_REQUESTS : FSPACES 'MaxRequests' FSPACES '='																	-> type( MAX_REQUESTS );
	 SETTINGS_REQUEST_TIMEOUT : FSPACES 'RequestTimeout' FSPACES '='															-> type( REQUEST_TIMEOUT );
	 SETTINGS_MAX_CONNECTIONS : FSPACES 'MaxConnections' FSPACES '='															-> type( MAX_CONNECTIONS );
	 SETTINGS_REQUEST_PAGE : FSPACES 'Page' FSPACES '='																			-> type( PAGE );
	 SETTINGS_REQUEST_RETRIES : FSPACES 'Retries' FSPACES '='																	-> type( RETRIES );
	 SETTINGS_REQUEST_RETRY_DELAY : FSPACES 'RetryDelay' FSPACES '='															-> type( RETRY_DELAY );
	 SETTINGS_PAGE : FSPACES FIDENT '.' FIDENT																					-> type( IDENT );
	 SETTINGS_NUMBER : FSPACES FNUMBER																							-> type( NUMBER );
	 SETTINGS_SEMICOLON : FSEMICOLON																							-> type( SEMICOLON );
	 SETTINGS_REQUEST_PATH : FSPACES 'Path' FSPACES '='																			-> type( PATH );
	 SETTINGS_REQUEST_BASE_PATH : FSPACES 'Base' FSPACES '='																	-> type( BASE_PATH );
	 SETTINGS_PATH: FSPACES FPATH_CHARACTERS+ '\''?																				-> type( PATH_NAME);
	 SETTINGS_LISTENER_RETRY_DELAY : FSPACES 'ListenerRetryDelay' FSPACES '='													-> type( LISTENER_RETRY_DELAY );
	 END_SETTINGS_MODE : FSPACES '}'																							-> type( END_SETTINGS ), popMode;

mode PROTOCOL_ATTRIBUTE_MODE;
	 SETTINGS_AUTH : FSPACES 'RequiresAuthori' ( 's' |'z' ) 'ation'																-> type( AUTH_TOKEN );
	 END_PROTOCOL_ATTRIBUTE : FSPACES ']'																						-> skip, popMode;

mode ATTRIBUTE_MODE;
	 ATTRIBUTE_MODE_AUTH : FSPACES 'Authori' ( 's' | 'z' ) 'ation'																-> type( AUTH_TOKEN );
	 ATTRIBUTE_NO_AUTH : FSPACES 'NoAuthori' ( 's' |'z' ) 'ation'																-> type( NO_AUTH_TOKEN );
	 ATTRIBUTE_COMMA: FCOMMA																									-> type( COMMA );
	 ATTRIBUTE_MODE_ATTRIBUTE : FSPACES FIDENT ( FSPACES ':' FSPACES ( ( FUP | FDOWN ) | (FUP FSPACES ',' FSPACES FDOWN ) ) )?	-> type( ATTRIBUTE );
	 END_ATTRIBUTE_MODE : FSPACES ']'																							-> skip, popMode;

	fragment FUP : ( 'U' | 'u' ) 'pload';
	fragment FDOWN : ( 'D' | 'd' ) 'ownload';

	fragment FPROPERTY : FIDENT ( '.' FIDENT )*;

	fragment FANY_CHAR : ~[\n\r];
	fragment FCOMMENT : FSPACES '/*' (FCOMMENT|.)*? '*/';
	fragment FLINE_COMMENT : FSPACES '//' FANY_CHAR* ( '\r' | '\n' );

	fragment FSUMMARY : FSPACES '<summary>' ( ' '..'~' | [\r\n\t] )*? '</summary>'  ;

	fragment AZ : ( 'A'..'Z' | 'a'..'z' );
	fragment AZ_ : ( AZ | '_' );
	fragment DIGIT : ( '0'..'9' );

	fragment FFORWARDER_ATTRIBUTE : AZ+ '/' (AZ | '-' )+ ;
	fragment FQUOTED_FORWARDER : '"' ( AZ_ | DIGIT | '*' | '.' | '/' )+ '"';

	fragment FQUOTED_IDENT : '"' FIDENT '"';
	fragment FIDENT : AZ_ ( AZ_ | DIGIT )*;
	fragment FNUMBER : (DIGIT | '_' )+;
	fragment FSPACES : FSPACE*;
	fragment FSPACE : ( ' ' | '\t' | '\r' | '\n' );
	fragment FPATH_CHARACTERS : ~[\u0000-\u002e\u003a-\u0040\u005b-\u0060\u007b-\uffff];		// Valid alpha numeric and '/'
	fragment FSEMICOLON : FSPACES ';';
	fragment FCOMMA : FSPACES ',';
